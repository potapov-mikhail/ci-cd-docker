FROM node:18.14.2-alpine

WORKDIR /opt/app
COPY package*.json ./
RUN npm ci
ADD . .
RUN npm run build
RUN npm prune --production

CMD [ "node", "./dist/index.js" ]
